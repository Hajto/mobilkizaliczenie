package com.jhajto.mobilkizaliczenie.services;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.jhajto.mobilkizaliczenie.ui.main.MainActivity;

public class ScreenOffReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF) &&
                context instanceof MainActivity &&
                !PersistentLoginManager.INSTANCE.autologoutDisabled(context)) {
            ((MainActivity) context).finish();
        }

    }

    public static ScreenOffReceiver registerNewScreenOffReceiver(Activity activity) {
        ScreenOffReceiver screenOffReceiver = new ScreenOffReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        activity.registerReceiver(screenOffReceiver, intentFilter);
        return screenOffReceiver;
    }
}
