package com.jhajto.mobilkizaliczenie.services

import android.content.Context

object PersistentLoginManager {
    const val LOGIN_KEY = "DISABLE_AUTOLOOGOUT"
    const val LOGGED_USER_ID = "LOGGED_USER_ID"
    val APPNAME = "appprefs"
    fun rembemberLoggedIn(context: Context, disabled:Boolean): Unit {
        val pref = context.getSharedPreferences(APPNAME, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putBoolean(LOGIN_KEY, disabled)
        editor.apply()
    }

    fun setLoggedUserId(context: Context, id: String) : Unit {
        val pref = context.getSharedPreferences(APPNAME, Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putString(LOGGED_USER_ID, id)
        editor.apply()
    }

    fun autologoutDisabled(context: Context): Boolean =
            context.getSharedPreferences(APPNAME, Context.MODE_PRIVATE).getBoolean(LOGIN_KEY, true)

    fun loggedInUser(context: Context) : String =
            context.getSharedPreferences(APPNAME, Context.MODE_PRIVATE).getString(LOGGED_USER_ID,"")!!
}