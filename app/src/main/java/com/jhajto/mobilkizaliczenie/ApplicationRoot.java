package com.jhajto.mobilkizaliczenie;

import android.app.Application;

import androidx.room.Room;

import com.jhajto.mobilkizaliczenie.data.AppDatabase;

public class ApplicationRoot extends Application {
    private AppDatabase db;


    @Override
    public void onCreate() {
        super.onCreate();
        this.db = Room.databaseBuilder(
                getApplicationContext(),
                AppDatabase.class, "database-name"
        )
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration().build();

    }

    public AppDatabase getDb() {
        return db;
    }
}
