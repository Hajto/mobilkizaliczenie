package com.jhajto.mobilkizaliczenie.data;

import com.jhajto.mobilkizaliczenie.data.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class InMemoryTaskRepository implements TaskRepository{
    private ArrayList<Task> tasks = new ArrayList<>();
    private int lastInsertedTaskId = 0;

    @Override
    public List<Task> getPendingTasks() {
        return filterTasks(new Predicate<Task>() {
            @Override
            public boolean test(Task task) {
                return !task.finished;
            }
        });
    }

    @Override
    public List<Task> getDoneTasks() {
        return filterTasks(new Predicate<Task>() {
            @Override
            public boolean test(Task task) {
                return task.finished;
            }
        });
    }

    @Override
    public void setTaskDone(Task task) {
        // This object is mutable so why not...
        task.finished = true;
    }

    @Override
    public void setTaskUndone(Task task) {
        // This object is mutable so why not...
        task.finished = false;
    }

    @Override
    public void insertTask(String text, String owner) {
        tasks.add(new Task(text, owner));
    }

    private List<Task> filterTasks(Predicate<Task> pred) {
        ArrayList<Task> tasksLocal = new ArrayList<>();
        for (Task task : tasks ){
            if(pred.test(task)){
                tasksLocal.add(task);
            }
        }

        return tasksLocal;
    }
}