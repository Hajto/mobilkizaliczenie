package com.jhajto.mobilkizaliczenie.data;

import com.jhajto.mobilkizaliczenie.data.model.Task;

import java.util.List;

public interface TaskRepository {
    List<Task> getPendingTasks();
    List<Task> getDoneTasks();
    void setTaskDone(Task task);
    void setTaskUndone(Task task);
    void insertTask(String text, String owner);
}
