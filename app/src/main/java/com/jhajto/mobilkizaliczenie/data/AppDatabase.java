package com.jhajto.mobilkizaliczenie.data;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.jhajto.mobilkizaliczenie.data.dao.TaskDao;
import com.jhajto.mobilkizaliczenie.data.dao.UserAuthDao;
import com.jhajto.mobilkizaliczenie.data.model.Task;
import com.jhajto.mobilkizaliczenie.data.model.UserCredentials;

@Database(entities = {UserCredentials.class, Task.class}, version = 3)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserAuthDao userDao();
    public abstract TaskDao taskDao();
}