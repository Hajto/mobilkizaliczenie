package com.jhajto.mobilkizaliczenie.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity
public class Task {
    public Task() {
    }

    public Task(String text, String owner) {
        this.taskName = text;
        this.usserAccountId = owner;
    }

    @PrimaryKey
    public Integer id;

    @ColumnInfo
    public String usserAccountId;

    @ColumnInfo
    public String taskName;
    @ColumnInfo
    public boolean finished = false;
}
