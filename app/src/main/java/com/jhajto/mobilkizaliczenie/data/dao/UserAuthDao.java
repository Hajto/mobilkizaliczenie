package com.jhajto.mobilkizaliczenie.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.jhajto.mobilkizaliczenie.data.model.UserCredentials;

@Dao
public interface UserAuthDao {
    @Query("SELECT * FROM user WHERE login LIKE :login AND " +
            "password LIKE :password LIMIT 1")
    UserCredentials findByName(String login, String password);

    @Insert
    long insertCredentials(UserCredentials userData);

}
