package com.jhajto.mobilkizaliczenie.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.jhajto.mobilkizaliczenie.data.model.Task;

import java.util.List;

@Dao
public interface TaskDao {
    @Query("SELECT * FROM Task where finished = 0 and usserAccountId = :userAccountID")
    List<Task> getListOfUnfinishedTasks(String userAccountID);

    @Query("SELECT * FROM Task where finished = 1 and usserAccountId = :userAccountID")
    List<Task> getListOfFinishedTasks(String userAccountID);

    @Insert
    void insertTask(Task task);

    @Update
    void updateTaskState(Task task);
}
