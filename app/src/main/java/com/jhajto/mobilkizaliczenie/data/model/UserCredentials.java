package com.jhajto.mobilkizaliczenie.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NonNls;

import java.util.UUID;

@Entity(tableName = "user", indices = {@Index(value="login", unique = true)})
public class UserCredentials {
    public UserCredentials(String login, String password) {
        this.login = login;
        this.password = password;
        this.uid = UUID.randomUUID().toString();
    }

    @PrimaryKey
    @NonNull
    public String uid;

    @ColumnInfo(name = "login")
    public String login;

    @ColumnInfo(name = "password")
    public String password;
}
