package com.jhajto.mobilkizaliczenie.ui.main;

import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.Tasks;
import com.google.android.material.tabs.TabLayout;
import com.jhajto.mobilkizaliczenie.ApplicationRoot;
import com.jhajto.mobilkizaliczenie.R;
import com.jhajto.mobilkizaliczenie.data.dao.TaskDao;
import com.jhajto.mobilkizaliczenie.data.model.Task;
import com.jhajto.mobilkizaliczenie.services.PersistentLoginManager;
import com.jhajto.mobilkizaliczenie.services.ScreenOffReceiver;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class MainActivity extends FragmentActivity implements NewTaskDialog.NoticeDialogListener, Observer {


    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;
    private TaskDao taskDao;
    private String loggedInUser;
    private Observable mObservers = new AlwaysChangedObservable();

    private BroadcastReceiver screenOffReceiver;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mObservers.addObserver(this);
        setContentView(R.layout.main_activity);
        loggedInUser = PersistentLoginManager.INSTANCE.loggedInUser(MainActivity.this);
        taskDao = ((ApplicationRoot) getApplication()).getDb().taskDao();
        Log.d("test how many records present found", String.valueOf(taskDao.getListOfUnfinishedTasks(loggedInUser).size()));

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(mPager);
        Log.d("dupa", "Registering receiver");
        //Register new receiver
        screenOffReceiver = ScreenOffReceiver.registerNewScreenOffReceiver(MainActivity.this);


        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment fragment = new NewTaskDialog();

                fragment.show(getSupportFragmentManager(), null);
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(screenOffReceiver);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        View editText = dialog.getDialog().findViewById(R.id.newTask);
        String newTaskContents = ((EditText) editText).getText().toString();
        taskDao.insertTask(new Task(newTaskContents, loggedInUser));
        mObservers.notifyObservers(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg != null)
            mPager.getAdapter().notifyDataSetChanged();
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        /**
         * The number of pages (wizard steps) to show in this demo.
         */
        private static final int NUM_PAGES = 2;


        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = TasksFragment.getInstance(position % 2 != 0);
            if (fragment != null) {
                Log.d("Not null", "fragment is not null");
                mObservers.addObserver((Observer) fragment);
            }

            mObservers.notifyObservers();
            return fragment;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "To Do";
            } else {
                return "Done";
            }
        }
    }

    private class AlwaysChangedObservable extends Observable {
        @Override
        public synchronized boolean hasChanged() {
            return true;
        }
    }
}
