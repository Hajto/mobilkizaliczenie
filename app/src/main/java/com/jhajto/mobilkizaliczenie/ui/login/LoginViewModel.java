package com.jhajto.mobilkizaliczenie.ui.login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.util.Patterns;

import com.jhajto.mobilkizaliczenie.data.Result;
import com.jhajto.mobilkizaliczenie.R;
import com.jhajto.mobilkizaliczenie.data.dao.UserAuthDao;
import com.jhajto.mobilkizaliczenie.data.model.UserCredentials;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private UserAuthDao loginRepository;
    private MutableLiveData<UserCredentials> state = new MutableLiveData<>();

    LoginViewModel(UserAuthDao loginRepository) {
        this.loginRepository = loginRepository;
    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    LiveData<UserCredentials> getLoginResult(){
        return state;
    }

    public void login(String username, String password) {
        // can be launched in a separate asynchronous job
        UserCredentials result = loginRepository.findByName(username, password);
        state.setValue(result);
    }

    public void loginDataChanged(String username, String password) {
        if (!isUserNameValid(username)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        if (username.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }
}
