package com.jhajto.mobilkizaliczenie.ui.login;

import android.app.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.jhajto.mobilkizaliczenie.ApplicationRoot;
import com.jhajto.mobilkizaliczenie.R;
//import com.jhajto.mobilkizaliczenie.data.AppDatabase;
import com.jhajto.mobilkizaliczenie.data.AppDatabase;
import com.jhajto.mobilkizaliczenie.data.model.UserCredentials;
import com.jhajto.mobilkizaliczenie.services.PersistentLoginManager;
import com.jhajto.mobilkizaliczenie.ui.main.MainActivity;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class LoginActivity extends AppCompatActivity {

    public static final int LOGIN_REQUEST_CODE = 101;
    private LoginViewModel loginViewModel;
    private GoogleSignInClient mGoogleSignInClient;
    private AppDatabase db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.db = ((ApplicationRoot) this.getApplication()).getDb();
        loginViewModel = new LoginViewModel(db.userDao());

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final Button registerButton = findViewById(R.id.register);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, new Observer<UserCredentials>() {
            @Override
            public void onChanged(@Nullable UserCredentials loginResult) {
                loadingProgressBar.setVisibility(View.GONE);
                if (loginResult == null) {
                    showLoginFailed(R.string.login_failed);
                } else {
                    setResult(Activity.RESULT_OK);
                    loginSuccess(loginResult.uid);
                }


            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginViewModel.login(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString());
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                loginViewModel.login(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String login = usernameEditText.getText().toString();
                final String password = passwordEditText.getText().toString();
                FutureTask<Long> task = new FutureTask<>(new Callable<Long>() {
                    @Override
                    public Long call() throws Exception {
                        return db.userDao().insertCredentials(new UserCredentials(login, password));
                    }
                });
                Executor executor = Executors.newSingleThreadExecutor();
                executor.execute(task);
                try {
                    String insertedId;
                    insertedId = String.valueOf(task.get(500, TimeUnit.MILLISECONDS));
                    loginSuccess(insertedId);
                    Log.d("Register status", insertedId);
                } catch (Exception e) {
                    Toast.makeText(LoginActivity.this, "Registration failed",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
        setupGoogleSignIn();
        findViewById(R.id.google_sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

    }

    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = getString(R.string.welcome) + model.getDisplayName();
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    private void setupGoogleSignIn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestId()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, LOGIN_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST_CODE) {
            Log.d("LOGIN", String.valueOf(requestCode));
        }
        if (resultCode == Activity.RESULT_OK)
            if (requestCode == LOGIN_REQUEST_CODE) {
                try {
                    // The Task returned from this call is always completed, no need to attach
                    // a listener.
                    Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    // TODO: Remove the null warning
                    if (account != null) {
                        Log.d("Login Success", account.getEmail());
                        Log.d("Login Success", account.getId());
                        loginSuccess(account.getId());
                    }
                } catch (ApiException e) {
                    // The ApiException status code indicates the detailed failure reason.
                    Log.w("LOGIN failed", "signInResult:failed code=" + e.getStatusCode());
                }
            }
    }

    private void loginSuccess(String userId) {
        CheckBox disableLogout = findViewById(R.id.disablelogout);
        PersistentLoginManager.INSTANCE.rembemberLoggedIn(LoginActivity.this,
                disableLogout.isChecked());
        PersistentLoginManager.INSTANCE.setLoggedUserId(LoginActivity.this, userId);

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
