package com.jhajto.mobilkizaliczenie.ui.main;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.recyclerview.widget.RecyclerView;

import com.jhajto.mobilkizaliczenie.R;
import com.jhajto.mobilkizaliczenie.data.model.Task;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TaskRecyclerViewAdapter extends RecyclerView.Adapter<TaskRecyclerViewAdapter.MyViewHolder> {
    private List<Task> dataSet;
    private OnItemCheckedListener onChecked;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        CheckBox checkBox;

        MyViewHolder(View v) {
            super(v);
            this.checkBox = v.findViewById(R.id.taskVerticalCheckbox);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    TaskRecyclerViewAdapter(List<Task> myDataset, OnItemCheckedListener listener) {
        dataSet = myDataset;
        onChecked = listener;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vertical_task_layout, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Log.d("refreshing position:", String.valueOf(position));
        Log.d("refreshing", dataSet.get(position).taskName);
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.checkBox.setText(dataSet.get(position).taskName);
        holder.checkBox.setChecked(dataSet.get(position).finished);
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                onChecked.onTodoClicked(dataSet.get(position));
                dataSet.remove(dataSet.get(position));
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public interface OnItemCheckedListener{
        public void onTodoClicked(Task task);
    }
}
