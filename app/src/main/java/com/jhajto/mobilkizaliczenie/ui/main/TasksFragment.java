package com.jhajto.mobilkizaliczenie.ui.main;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jhajto.mobilkizaliczenie.ApplicationRoot;
import com.jhajto.mobilkizaliczenie.R;
import com.jhajto.mobilkizaliczenie.data.dao.TaskDao;
import com.jhajto.mobilkizaliczenie.data.model.Task;
import com.jhajto.mobilkizaliczenie.services.PersistentLoginManager;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class TasksFragment extends Fragment implements TaskRecyclerViewAdapter.OnItemCheckedListener, Observer {
    private TaskDao dao;
    private boolean isFinishedFilter;
    private List<Task> tasks;
    private RecyclerView.Adapter taskRecyclerViewAdapter;
    private String userId;
    private Observable dataChanged;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        dao = ((ApplicationRoot) getActivity().getApplication()).getDb().taskDao();


        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.task_scroll_view, container, false);
        RecyclerView recyclerView = rootView.findViewById(R.id.taskRecycler);
        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        isFinishedFilter = getArguments().getBoolean("isFinishedFilter");

        // specify an adapter (see also next example)
        userId = PersistentLoginManager.INSTANCE.loggedInUser(getContext());
        tasks = getTasks(userId);
        Log.d("Test fragment", String.valueOf(tasks.size()));
        taskRecyclerViewAdapter = new TaskRecyclerViewAdapter(tasks, this);
        recyclerView.setAdapter(taskRecyclerViewAdapter);

        return rootView;
    }


    private List<Task> getTasks(String userId) {
        return isFinishedFilter
                ? dao.getListOfFinishedTasks(userId)
                : dao.getListOfUnfinishedTasks(userId);
    }


    public static TasksFragment getInstance(boolean isFinishedFilter) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isFinishedFilter", isFinishedFilter);
        TasksFragment tasksFragment = new TasksFragment();
        tasksFragment.setArguments(bundle);
        return tasksFragment;
    }

    @Override
    public void onTodoClicked(Task task) {
        task.finished = !task.finished;
        dao.updateTaskState(task);
        if (dataChanged != null)
            dataChanged.notifyObservers(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        this.dataChanged = o;
    }
}

